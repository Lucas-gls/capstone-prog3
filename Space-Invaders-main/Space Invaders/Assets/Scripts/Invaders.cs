using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class InvadersGrid : MonoBehaviour
{
    public Invader[] prefabs;
    public Projectile missilePrefab;
    public int rows = 5;
    public int columns = 11;
    public AnimationCurve speed;
    public float MissileAttackRate = 0.002f;
    public AudioClip deathSound;
    public TextMeshProUGUI scoreText;

    private float nextShootTime = 0f;
    private float initialShootInterval = 2f;
    private float shootIntervalDecrease = 0.02f;

    public int amountKilled { get; private set;}
    public int amountAlive => totalInvaders - amountKilled;
    public int totalInvaders => rows * columns;
    public float percentKilled => (float) amountKilled / (float) totalInvaders;
    public float horizontalSpacing = 1f;
    public float verticalSpacing = 0f;

    private Vector3 _direction = Vector2.right;
    private AudioSource _audioSource;
    private Vector3[,] initialPositions;
    private Vector3 startPosition; 

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        float horizontalSpacing = 2.5f;
        float verticalSpacing = 1.7f;

        initialPositions = new Vector3[rows, columns];

        startPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.8f, 10.0f));

        for (int row = 0; row < this.rows; row++)
        {
            float width = (this.columns - 1) * horizontalSpacing;
            float height = (this.rows - 1) * verticalSpacing;

            Vector2 centering = new Vector2(-width / 2, -height / 2);
            Vector3 rowPosition = new Vector3(centering.x, centering.y + (row * verticalSpacing), 0.0f);

            for (int column = 0; column < this.columns; column++)
            {
                Invader i = Instantiate(this.prefabs[row % this.prefabs.Length], this.transform);
                i.invaderDestroyed += OnInvaderKilled;
                i.row = row;
                Vector3 position = rowPosition;
                position.x += column * horizontalSpacing;
                i.transform.localPosition = position;

                initialPositions[row, column] = position;
            }
        }
    }

    private void Start()
    {
        ScoreManager.score = 0;
        UpdateScore(ScoreManager.score);
        InvokeRepeating(nameof(MissileAttack), this.MissileAttackRate, this.MissileAttackRate);
    }

    private void Update()
    {
        this.transform.position += _direction * this.speed.Evaluate(this.percentKilled) * Time.deltaTime;

        Vector3 leftEdge = Camera.main.ViewportToWorldPoint(Vector3.zero);
        Vector3 rightEdge = Camera.main.ViewportToWorldPoint(Vector3.right);
        
        foreach (Transform invader in this.transform)
        {
            if (!invader.gameObject.activeInHierarchy) {
                continue;
            }

            if (_direction == Vector3.right && invader.position.x >= rightEdge.x - 1.0f) 
            {
                AdvanceRow();
            } else if (_direction == Vector3.left && invader.position.x <= leftEdge.x + 1.0f) 
            {
                AdvanceRow();
            }
        }
        if (Random.value < (1.0f / (float)this.amountAlive))
            RandomInvaderShoot(); 
    }

    private void AdvanceRow()
    {
        _direction.x *= -1.0f;

        Vector3 position = this.transform.position;
        position.y -= 1.0f;
        this.transform.position = position;
    }

    private void MissileAttack()
    {
        float attackRate = Random.Range(2.0f, 5.0f); 
        float missileSpeed = Random.Range(5.0f, 10.0f); 

        foreach (Transform invader in this.transform)
        {
            if (!invader.gameObject.activeInHierarchy) {
                continue;
            }

            if (Random.value < (1.0f / (float)this.amountAlive))
            {
                Projectile missile = Instantiate(this.missilePrefab, invader.position, Quaternion.identity);
                missile.speed = missileSpeed; 
                break;
            }

        }
        Invoke(nameof(MissileAttack), attackRate + 10.5f);
    }

    private void OnInvaderKilled(int row)
    {
        _audioSource.PlayOneShot(this.deathSound);

        amountKilled++;

        switch (row)
        {
            case 4:
                    ScoreManager.score += 40;
                break;
            case 3:
            case 2:
                    ScoreManager.score += 20;
                break;
            case 1:
            case 0:
                    ScoreManager.score += 10;
                break;
        }

        UpdateScore(ScoreManager.score);

        if (this.amountKilled >= this.totalInvaders)
        {
            ResetInvaders();
        }
        else if (this.amountKilled % this.totalInvaders == 0)
        {
            ResetInvaders();
        }
        else if (this.amountKilled >= this.totalInvaders - 1)
        {
            ResetInvaders();
        }
    }

    private void ResetInvaders()
    {
        this.transform.position = startPosition;

        for (int row = 0; row < rows ; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                int index = row * columns + column;
                Transform invader = transform.GetChild(index);
                invader.gameObject.SetActive(true);
                invader.localPosition = initialPositions[row, column];
                invader.GetComponent<SpriteRenderer>().sprite = prefabs[row % prefabs.Length].GetComponent<SpriteRenderer>().sprite;
            }
        }
        amountKilled = 0;

        UpdateScore(ScoreManager.score);
    }

    private void RandomInvaderShoot()
    {
        if (Time.time > nextShootTime)
        {
            Transform randomInvader = GetRandomActiveInvader();

            if (randomInvader != null)
            {
                Instantiate(this.missilePrefab, randomInvader.position, Quaternion.identity);
            }

            UpdateScore(ScoreManager.score);

            // Diminuir o intervalo de disparo
            initialShootInterval -= shootIntervalDecrease;
            initialShootInterval = Mathf.Max(0.1f, initialShootInterval); // Garante que o intervalo n�o seja menor que 0.1 segundo

            nextShootTime = Time.time + initialShootInterval; // Define o pr�ximo tempo de disparo
        }
    }

    private Dictionary<float, List<Transform>> GroupInvadersByYPosition()
    {
        Dictionary<float, List<Transform>> rows = new Dictionary<float, List<Transform>>();

        foreach (Transform invader in this.transform)
        {
            if (invader.gameObject.activeInHierarchy)
            {
                float yPos = invader.position.y;
                if (!rows.ContainsKey(yPos))
                {
                    rows[yPos] = new List<Transform>();
                }
                rows[yPos].Add(invader);
            }
        }

        return rows;
    }

    private float FindFrontLineYPosition(Dictionary<float, List<Transform>> rows)
    {
        float frontLineYPos = float.MinValue;

        foreach (var yPos in rows.Keys)
        {
            if (yPos > frontLineYPos)
            {
                frontLineYPos = yPos;
            }
        }

        return frontLineYPos;
    }

    private Transform GetRandomInvaderFromLine(float yPos, Dictionary<float, List<Transform>> rows)
    {
        if (rows.ContainsKey(yPos) && rows[yPos].Count > 0)
        {
            List<Transform> lineInvaders = rows[yPos];
            return lineInvaders[UnityEngine.Random.Range(0, lineInvaders.Count)];
        }

        return null;
    }

    private Transform GetRandomActiveInvader()
    {
        Dictionary<float, List<Transform>> rows = GroupInvadersByYPosition();
        float frontLineYPos = FindFrontLineYPosition(rows);

        return GetRandomInvaderFromLine(frontLineYPos, rows);
    }

    private void UpdateScore(int pontos)
    {
        ScoreManager.score = pontos;

        if (scoreText != null)
        {
            scoreText.text = "Score: " + ScoreManager.score.ToString() + " Lives: " + Player.lives.ToString();
            Debug.Log("Score and lives updated: " + scoreText.text);
        }
        else
        {
            Debug.LogError("scoreText is not assigned!");
        }
    }
}