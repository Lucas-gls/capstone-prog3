
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void PlayGame()
    {
        if(ScoreManager.PlayerName != "")
        {
            Player.lives = 3;
            Time.timeScale = 1;
            SceneManager.LoadScene("GameScreen");
        }
    }

    public void ScoreScene()
    {
        SceneManager.LoadScene("ScoresScene");
    }

    public void ReturnMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuScreen");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
