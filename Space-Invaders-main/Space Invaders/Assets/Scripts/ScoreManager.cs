using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class ScoreManager : MonoBehaviour
{
    private ScoreData sd;
    public static string PlayerName = "";
    public static int score = 0;

    public void GetText(string text)
    {
        PlayerName = text;
    }

    void Awake()
    {
        //PlayerPrefs.DeleteKey("scores");
        var json = PlayerPrefs.GetString("scores", "{}");
        sd = JsonUtility.FromJson<ScoreData>(json);
    }

    public IEnumerable<Score> GetHighScore()
    {
        return sd.scores.OrderByDescending(x => x.score);
    }

    public void AddScore(Score score)
    {
        sd.scores.Add(score);
    }

    private void OnDestroy()
    {
        SaveScore();
    }
    
    public void SaveScore()
    {
        var json = JsonUtility.ToJson(sd);
        PlayerPrefs.SetString("scores", json);
    }
}
